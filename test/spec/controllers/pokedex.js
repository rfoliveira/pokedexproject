'use strict';

describe('Controller: PokedexCtrl', function() {

	// load the controller's module
	beforeEach(module('pokedexProjectApp'));

	var PokemonCtrl,
		scope;

	// Initialize the controller and a mock scope
	beforeEach(inject(function($controller, $rootScope) {
		scope = $rootScope.$new();
		PokedexCtrl = $controller('PokedexCtrl', {
			$scope: scope
			// place here mocked dependencies
		});

		it('should get the list of pokemons available', function () {
			scope.getPokemonsList();
		    expect(scope.pokemons.length).not.toBe(0);
		});

		it('should get the data of a pokemon named scyther', function () {
			scope.getPokemonData('pokemon/123/');
		    expect(scope.pokemon.name).toEqual("Scyther");
		});



		it('should increase the number of comments', function () {
			scope.getPokemonData('pokemon/123/');
			var actualSize = $scope.dataSet.comments.length;

			scope.forms = {
				name:"Teste",
				email:"rfoliveira@gmail.com",
				comment: "Oi",
				pokemon: "123"
			};

			scope.submitComment(true);

		    expect($scope.dataSet.comments.length).not.toBe(actualSize);
		});
	}));
});