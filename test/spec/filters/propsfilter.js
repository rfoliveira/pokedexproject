'use strict';

describe('Filter: propsFilter', function() {

	// load the filter's module
	beforeEach(module('pokedexProjectApp'));

	// initialize a new instance of the filter before each test
	var propsFilter;
	beforeEach(inject(function($filter) {
		propsFilter = $filter('propsFilter');
	}));

	it('should return an array with itens according to the search filter:"', function() {
		var text = 'bulba';
		expect(propsFilter(text).length).not.toBe(0);
	});

});