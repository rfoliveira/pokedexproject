# pokedex-project

This project is generated with [yo angular generator](https://github.com/yeoman/generator-angular)
version 1.0.0.

## Build & development

First run `npm install`, to install all dependencies of the project.

Then run `bower install`.

And finally run `grunt` for building and `grunt serve` for preview.

## Testing

Running `grunt test` will run the unit tests with karma.
