'use strict';

angular.module('pokedexProjectApp')
  .filter('reverse', function() {
  	return function(items) {
  		return angular.isArray(items) ? items.slice().reverse() : [];
  	};
  });