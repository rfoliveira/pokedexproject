'use strict';

/**
 * @ngdoc function
 * @name pokedexProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pokedexProjectApp
 */
angular.module('pokedexProjectApp')
	.controller('PokedexCtrl', ['$scope', 'pokedexFactory', 'Ref', '$firebaseObject' ,
		function($scope, pokedexFactory, Ref, $firebaseObject) {
			
			var syncObject = $firebaseObject(Ref);


			syncObject.$bindTo($scope, "dataSet");



			$scope.getPokemonsList = function() {
				pokedexFactory.getPokemonsList()
					.success(function(data) {
						$scope.pokemons = data.pokemon;
					})
					.error(function(error) {
						$scope.status = error.message;
					});
			};

			$scope.getPokemonsList();


			$scope.getPokemonData = function(resourceUri) {
				$scope.loading = true;
				delete $scope.pokemon;
				pokedexFactory.getDataByUri(resourceUri)
					.success(function(data) {
						$scope.pokemon = data;
						if ($scope.pokemon.sprites.length > 0) {
							$scope.getPokemonImage($scope.pokemon.sprites[0].resource_uri);
						}else{
							$scope.loading = false;
						}
					})
					.error(function(error) {
						$scope.status = error.message;
						$scope.loading = false;
					});
			};

			$scope.getPokemonImage = function(resourceUri) {
				pokedexFactory.getDataByUri(resourceUri)
					.success(function(data) {
						$scope.pokemon.image = data.image;
						$scope.loading = false;
					})
					.error(function(error) {
						$scope.status =  error.message;
						$scope.loading = false;
					});
			};


			$scope.submitComment = function(isValid) {

				if(!isValid){
					alert("All fields are required!");
				}else{

					if($scope.dataSet.comments==undefined){
						$scope.dataSet.comments=[];
					}
					$scope.forms.pokemon = $scope.pokemon.pkdx_id;

					$scope.dataSet.comments.push($scope.forms);

					delete $scope.forms;
				}
			};
		}
	]);