angular.module('firebase.config', [])
  .constant('FBURL', 'https://pokedexcomments.firebaseio.com')
  .constant('SIMPLE_LOGIN_PROVIDERS', ['anonymous'])

  .constant('loginRedirectPath', '/login');
