'use strict';

/**
 * @ngdoc overview
 * @name pokedexProjectApp
 * @description
 * # pokedexProjectApp
 *
 * Main module of the application.
 */
angular.module('pokedexProjectApp', [
	'ngAnimate',
	'ngCookies',
	'ngResource',
	'ngRoute',
	'ngSanitize',
	'ngTouch',
	'firebase',
	'firebase.ref',
	'firebase.auth',
	'ui.select'
]);