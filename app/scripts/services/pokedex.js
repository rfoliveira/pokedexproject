'use strict';

/**
 * @ngdoc function
 * @name pokedexProjectApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pokedexProjectApp
 */
angular.module('pokedexProjectApp')
	.factory('pokedexFactory', function($http) {

		var urlBase = 'http://pokeapi.co/';
		var pokedexFactory = {};
		//
		pokedexFactory.getPokemonsList = function() {
			return $http.get(urlBase + 'api/v1/pokedex/1');
		};

		//
		pokedexFactory.getDataByUri = function(resourceUri) {
			return $http.get(urlBase + resourceUri);
		};

		return pokedexFactory;
	});